﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Models;

namespace Bll.Interfaces
{
    public interface IStoreBll
    {
        List<StoreDto> GetAllStories();

        void AddStore(StoreDto store);
    }
}
