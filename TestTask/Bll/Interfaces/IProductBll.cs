﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Models;

namespace Bll.Interfaces
{
    public interface IProductBll
    {
        void DeleteProduct(ProductDto product);

        void AddProduct(ProductDto product);
    }
}
