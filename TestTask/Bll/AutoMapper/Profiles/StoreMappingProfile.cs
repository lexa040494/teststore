﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Model;
using ViewModel.Models;

namespace Bll.AutoMapper.Profiles
{
    public class StoreMappingProfile : Profile
    {
        public StoreMappingProfile()
        {
            CreateMap<Store, StoreDto>()
                .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(d => d.Address, opts => opts.MapFrom(src => src.Address))
                .ForMember(d => d.TimeFrom, opts => opts.MapFrom(src => src.TimeFrom))
                .ForMember(d => d.TimeTo, opts => opts.MapFrom(src => src.TimeTo))
                .ForMember(d => d.Products, opts => opts.MapFrom(src => src.Product))
                .ForMember(d => d.Days, opts => opts.MapFrom(src => src.WorkingDays));

            CreateMap<StoreDto, Store>()
                .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(d => d.Address, opts => opts.MapFrom(src => src.Address))
                .ForMember(d => d.TimeFrom, opts => opts.MapFrom(src => src.TimeFrom))
                .ForMember(d => d.TimeTo, opts => opts.MapFrom(src => src.TimeTo))
                .ForMember(d => d.Product, opts => opts.MapFrom(src => src.Products))
                .ForMember(d => d.WorkingDays, opts => opts.MapFrom(src => src.Days));
        }
    }
}
