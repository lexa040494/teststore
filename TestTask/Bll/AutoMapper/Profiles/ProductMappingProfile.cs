﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Model;
using ViewModel.Models;

namespace Bll.AutoMapper.Profiles
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<Product, ProductDto>()
               .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
               .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
               .ForMember(d => d.Description, opts => opts.MapFrom(src => src.Description))
               .ForMember(d => d.StoreId, opts => opts.MapFrom(src => src.StoreId));

            CreateMap<ProductDto, Product>()
               .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
               .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
               .ForMember(d => d.Description, opts => opts.MapFrom(src => src.Description))
               .ForMember(d => d.StoreId, opts => opts.MapFrom(src => src.StoreId));
        }
    }
}
