﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bll.Interfaces;
using Dal.Factory.Interface;
using Model;
using ViewModel.Models;

namespace Bll.Repositories
{
    public class StoreBll : IStoreBll
    {
        private readonly IDalFactory _dalFactory;

        public StoreBll(IDalFactory dalFactory)
        {
            _dalFactory = dalFactory;
        }

        public List<StoreDto> GetAllStories()
        {
            try
            {
                var storeList = _dalFactory.StoreDal.GetAll();
                return Mapper.Map<List<Store>, List<StoreDto>>(storeList.ToList());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        public void AddStore(StoreDto store)
        {
            try
            {
                var storeItem = Mapper.Map<StoreDto, Store>(store);
                _dalFactory.StoreDal.AddWithReturn(storeItem);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
