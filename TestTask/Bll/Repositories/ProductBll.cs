﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bll.Interfaces;
using Dal.Factory.Interface;
using Model;
using ViewModel.Models;

namespace Bll.Repositories
{
    public class ProductBll : IProductBll
    {
        private readonly IDalFactory _dalFactory;

        public ProductBll(IDalFactory dalFactory)
        {
            _dalFactory = dalFactory;
        }

        public void DeleteProduct(ViewModel.Models.ProductDto product)
        {
            _dalFactory.ProductDal.Delete(Mapper.Map<ProductDto, Product>(product));
        }

        public void AddProduct(ViewModel.Models.ProductDto product)
        {
            _dalFactory.ProductDal.AddWithReturn(Mapper.Map<ProductDto, Product>(product));
        }
    }
}
