﻿using Bll.Factory.Interface;
using Bll.Interfaces;
using Bll.Repositories;
using Dal.Factory.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll.Factory.Repository
{
    public class BllFactory : IBllFactory
    {
        private readonly IDalFactory _dalFactory;

        public BllFactory(IDalFactory dalFactory)
        {
            _dalFactory = dalFactory;
        }

        private IStoreBll _storeBll;

        public IStoreBll StoreBll
        {
            get { return _storeBll ?? (_storeBll = new StoreBll(_dalFactory)); }
        }

        private IProductBll _productBll;

        public IProductBll ProductBll
        {
            get { return _productBll ?? (_productBll = new ProductBll(_dalFactory)); }
        }
    }
}
