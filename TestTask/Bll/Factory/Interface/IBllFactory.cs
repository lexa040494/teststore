﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bll.Interfaces;

namespace Bll.Factory.Interface
{
    public interface IBllFactory
    {
        IStoreBll StoreBll { get; }
        IProductBll ProductBll { get; }
    }
}
