﻿using Dal.Factory.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Interfaces;
using Dal.Repositories;
using Model;

namespace Dal.Factory.Repository
{
    public class DalFactory : IDalFactory
    {
        private StoreManagerEntities _dbContext;
        private readonly IDbFactory _dbFactory;
        public DalFactory(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public StoreManagerEntities DbContext
        {
            get { return _dbContext ?? (_dbContext = _dbFactory.Init()); }
        }

        public void Dispose()
        {
            _dbContext.Dispose();

            GC.SuppressFinalize(this);
        }

        public IStoreDal _storeDal { get; private set; }

        public IStoreDal StoreDal
        {
            get { return _storeDal ?? (_storeDal = new StoreDal(_dbFactory)); }
        }

        public IProductDal _productDal { get; private set; }

        public IProductDal ProductDal
        {
            get { return _productDal ?? (_productDal = new ProductDal(_dbFactory)); }
        }
    }
}
