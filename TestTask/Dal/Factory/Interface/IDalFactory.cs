﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Interfaces;

namespace Dal.Factory.Interface
{
    public interface IDalFactory
    {
        IStoreDal StoreDal { get; }
        IProductDal ProductDal { get; }
    }
}
