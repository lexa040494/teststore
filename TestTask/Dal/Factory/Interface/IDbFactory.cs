﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Factory.Repository
{
    public interface IDbFactory
    {
        StoreManagerEntities Init();
    }
}
