﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Factory.Repository;
using Dal.Interfaces;
using Model;
using Dal.GenericRepository.Repository;

namespace Dal.Repositories
{
    public class ProductDal : GenericRepository<Product>, IProductDal
    {
        public ProductDal(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
