﻿/// <reference path="appConfig.js" />
(function () {
    "use strict";

    angular
        .module("Web")
        .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
            // Index
            $stateProvider.state("/store", {
                url: "/store",
                templateUrl: function () { return "Angular/catalog/store/store.html?" + new Date(); },
                controller: "storeController"
            });
        }]);
})();