﻿(function () {
    "use strict";

    function productService($cookies, $http, $rootScope, $q) {
        var service = {};

        service.DeleteProduct = function (product) {
            var deferred = $q.defer();

            $http.put('api/product/DeleteProduct', product)
           .then(function (response) {
               deferred.resolve(response.data);
           }).catch(function onError(response) {
               deferred.reject(response.data);
           });

            return deferred.promise;

        };

        service.AddProduct = function (product) {
            var deferred = $q.defer();

            $http.post('api/product/AddProduct', product)
           .then(function (response) {
               deferred.resolve(response.data);
           }).catch(function onError(response) {
               deferred.reject(response.data);
           });

            return deferred.promise;

        };

        return service;
    };

    angular
        .module("Web.Services")
        .service("productService", ["$cookies", "$http", "$rootScope", "$q", productService]);
})();
