﻿(function () {
    "use strict";

    function storeService($cookies, $http, $rootScope, $q) {
        var service = {};

        service.GetAllStories = function () {
            var deferred = $q.defer();

            $http.get('api/store/GetAllStories')
           .then(function (response) {
               deferred.resolve(response.data);
           }).catch(function onError(response) {
               deferred.reject(response.data);
           });

            return deferred.promise;

        };

        service.AddStore = function (store) {
            var deferred = $q.defer();

            store.TimeFrom = store.TimeFrom.toLocaleTimeString();
            store.TimeTo = store.TimeTo.toLocaleTimeString();

            $http.post('api/store/AddStore', store)
           .then(function (response) {
               deferred.resolve(response.data);
           }).catch(function onError(response) {
               deferred.reject(response.data);
           });

            return deferred.promise;

        };

        return service;
    };

    angular
        .module("Web.Services")
        .service("storeService", ["$cookies", "$http", "$rootScope", "$q", storeService]);
})();
