﻿/// <reference path="topMenuDirective/topMenu.js" />
(function () {
    "use strict";

    // controller class definintion
    var storeController = function ($scope, $rootScope, storeService, productService, uiGridConstants, $uibModal) {
        $scope.loadingInfo = true;

        $scope.GetAllStories = function () {
            storeService.GetAllStories().then(function (response) {
                $scope.gridStore.data = response;
            }, function (errorObject) {
                $rootScope.toaster('error', errorObject.ExceptionMessage, 4000);
            }).finally(function () {
                $scope.loadingInfo = false;
            });
        };

        $scope.GetAllStories();

        $scope.gridStore = {
            enableColumnResizing: true,
            showGridFooter: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 1,
            enableColumnMenus: false,
            showColumnFooter: true,
            enableFiltering: true,
            selectionRowHeaderWidth: 25,
            enableRowSelection: true,
            enableSelectAll: true,
            multiSelect: true,
            rowHeight: 25,
            headerRowHeight: 20,
            columnDefs: [
                {
                    field: 'Name',
                    width: '20%',
                    displayName: 'Название магазина',
                    headerTooltip: true,
                    enableFiltering: false
                },
                {
                    field: 'Address',
                    displayName: 'Адрес',
                    headerTooltip: true,
                    enableFiltering: false
                },
                {
                    field: 'Days',
                    displayName: 'Дни работы',
                    width: '20%',
                    headerTooltip: true,
                    enableFiltering: false
                },
                {
                    field: 'TimeFrom',
                    displayName: 'Начало работы',
                    width: '15%',
                    headerTooltip: true,
                    enableFiltering: false
                },
                {
                    field: 'TimeTo',
                    displayName: 'Окончание работы',
                    width: '15%',
                    headerTooltip: true,
                    enableFiltering: false,
                },
                {
                    field: 'Add',
                    displayName: '',
                    width: '5%',
                    headerTooltip: true,
                    enableFiltering: false,
                    cellTemplate: 'Angular/catalog/store/template/showListProduct.html',
                }
            ]
        };

        $scope.openModalProducts = function (entity) {
            $uibModal.open({
                templateUrl: function() { return 'Angular/catalog/store/modal/productList.html?' + new Date() },
                size: 'lg',
                scope: $scope,
                controller: [
                    '$rootScope', '$scope', function ($rootScope, $scope) {
                        $scope.loadingInfoData = true;
                        $scope.entity = entity;

                        $scope.gridProducts = {
                            enableColumnResizing: true,
                            showGridFooter: false,
                            enableHorizontalScrollbar: 0,
                            enableVerticalScrollbar: 1,
                            enableColumnMenus: false,
                            showColumnFooter: true,
                            enableFiltering: true,
                            selectionRowHeaderWidth: 25,
                            enableRowSelection: true,
                            enableSelectAll: true,
                            multiSelect: true,
                            rowHeight: 25,
                            headerRowHeight: 20,
                            columnDefs: [
                                {
                                    field: 'Name',
                                    width: '45%',
                                    displayName: 'Название товара',
                                    headerTooltip: true,
                                    enableFiltering: false
                                },
                                {
                                    field: 'Description',
                                    width: '45%',
                                    displayName: 'Описание',
                                    headerTooltip: true,
                                    enableFiltering: false
                                },
                                {
                                    field: 'DeleteProduct',
                                    displayName: '',
                                    width: '5%',
                                    headerTooltip: true,
                                    enableFiltering: false,
                                    cellTemplate: 'Angular/catalog/product/template/removeProduct.html',
                                }
                            ]
                        };

                        $scope.gridProducts.data = entity.Products;

                        $scope.loadingInfoData = false;

                        $scope.openModalAddProduct = function() {
                                $uibModal.open({
                                    templateUrl: function(){return 'Angular/catalog/product/modal/newProduct.html?' + new Date()},
                                size: 'md',
                                scope: $scope,
                                controller: [
                                    '$rootScope', '$scope', '$uibModalInstance', function ($rootScope, $scope, $uibModalInstance) {
                                        $scope.currentProduct = {};

                                        $scope.canselModal = function () {
                                            $uibModalInstance.dismiss({ $value: 'cancel' });
                                        };

                                        $scope.saveProduct = function () {
                                            $uibModalInstance.close($scope.currentProduct);
                                        };
                                    }
                                ]
                            }).result.then(function (result) {
                                result.StoreId = entity.Id;
                                productService.AddProduct(result).then(function () {
                                    entity.Products.push(result);
                                    $rootScope.toaster('success', 'Новый товар был успешно добавлен.', 4000);
                                }, function (errorObject) {
                                    $rootScope.toaster('error', errorObject.ExceptionMessage, 4000);
                                }).finally(function () {
                                    $scope.loadingInfo = false;
                                });
                            });
                        };

                        $scope.deleteProduct = function (item) {
                            productService.DeleteProduct(item).then(function () {

                                var index = entity.Products.findIndex(function (element, index, array) {
                                    if (element.Id == item.Id)
                                        return true;
                                    return false;
                                });

                                entity.Products.splice(index, 1);

                                $rootScope.toaster('success', 'Выбранный товар был успешно удален.', 4000);
                            }, function (errorObject) {
                                $rootScope.toaster('error', errorObject.ExceptionMessage, 4000);
                            }).finally(function () {
                            });
                        };
                    }
                ]
            });
        };



        $scope.openModalAddStore = function () {
            $uibModal.open({
                templateUrl: function() { return  'Angular/catalog/store/modal/newStore.html?' + new Date()},
                size: 'md',
                scope: $scope,
                controller: [
                    '$rootScope', '$scope', '$uibModalInstance', function ($rootScope, $scope, $uibModalInstance) {
                        $scope.currentStore = {};
                        $scope.currentStore.TimeFrom = new Date(1970, 0, 1, 8, 0, 0);
                        $scope.currentStore.TimeTo = new Date(1970, 0, 1, 22, 0, 0);

                        $scope.canselModal = function () {
                            $uibModalInstance.dismiss({ $value: 'cancel' });
                        };

                        $scope.saveStore = function () {
                            $uibModalInstance.close($scope.currentStore);
                        };
                    }
                ]
            }).result.then(function (result) {
                storeService.AddStore(result).then(function () {
                    $scope.loadingInfo = true;
                    $rootScope.toaster('success', 'Магазин ' + result.Name + ' был успешно добавлен.', 4000);
                    $scope.GetAllStories();
                }, function (errorObject) {
                    $rootScope.toaster('error', errorObject.ExceptionMessage, 4000);
                }).finally(function () {
                    $scope.loadingInfo = false;
                });
            });
        };
    };

    // register your controller into a dependent module 
    angular
        .module("Web.Controllers")
        .controller("storeController", ["$scope", "$rootScope", "storeService", "productService", "uiGridConstants", "$uibModal", storeController]);

})();