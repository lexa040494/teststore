﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Web.Controllers.BaseApi
{
    public class BaseApiController : ApiController
    {
        private JavaScriptSerializer _serializer;

        public BaseApiController()
        {
            _serializer = new JavaScriptSerializer();
        }

    }
}