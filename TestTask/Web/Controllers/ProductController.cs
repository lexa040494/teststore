﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Bll.Factory.Interface;
using Bll.Interfaces;
using ViewModel.Models;
using Web.Controllers.BaseApi;

namespace Web.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : BaseApiController
    {
        private readonly IProductBll _productBll;

        public ProductController(IBllFactory bllFactory)
        {
            if (bllFactory == null)
            {
                throw new ArgumentNullException("factoryBll");
            }
            _productBll = bllFactory.ProductBll;
        }

        [Route("AddProduct")]
        [HttpPost]
        public void AddProduct([FromBody] ProductDto product)
        {
            _productBll.AddProduct(product);
        }

        [Route("DeleteProduct")]
        [HttpPut]
        public void DeleteProduct([FromBody] ProductDto product)
        {
            _productBll.DeleteProduct(product);
        }
    }
}