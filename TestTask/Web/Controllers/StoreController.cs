﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Bll.Factory.Interface;
using Bll.Interfaces;
using ViewModel.Models;
using Web.Controllers.BaseApi;

namespace Web.Controllers
{
     [RoutePrefix("api/store")]
    public class StoreController : BaseApiController
    {
        private readonly IStoreBll _storeBll;

        public StoreController(IBllFactory bllFactory)
        {
            if (bllFactory == null)
            {
                throw new ArgumentNullException("factoryBll");
            }
            _storeBll = bllFactory.StoreBll;
        }

        [Route("GetAllStories")]
        [HttpGet]
        public List<StoreDto> GetAllStories()
        {
            var storeList = _storeBll.GetAllStories();
            return storeList;
        }

        [Route("AddStore")]
        [HttpPost]
        public void AddStore([FromBody] StoreDto store)
        {
            _storeBll.AddStore(store);
        }
    }
}